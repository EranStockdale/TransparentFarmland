package uk.eranstockdale.transparentfarmland;

import com.mojang.logging.LogUtils;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.slf4j.Logger;
import uk.eranstockdale.transparentfarmland.block.ModBlocks;
import uk.eranstockdale.transparentfarmland.item.ModItems;

@Mod(TransparentFarmland.MOD_ID)
public class TransparentFarmland {
    public static final String MOD_ID = "transparentfarmland";

    private static final Logger LOGGER = LogUtils.getLogger();

    public TransparentFarmland() {
        IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();
        eventBus.addListener(this::setup); // register the setup method for modloading
        eventBus.addListener(this::clientSetup); // register the client setup method for modloading

        ModItems.register(eventBus);
        ModBlocks.register(eventBus);

        MinecraftForge.EVENT_BUS.register(this); // register for events
    }

    private void setup(final FMLCommonSetupEvent event) {}

    private void clientSetup(final FMLClientSetupEvent event) {
        ItemBlockRenderTypes.setRenderLayer(ModBlocks.GLIRT.get(), RenderType.translucent());
    }
}